## Introduction

Prince of Songkla University (PSU) was established in 1967 as the first university in southern Thailand. The original aims of the university were to raise the general education standards and support regional industry and development.

Today, PSU is a leading public university, committed to academic excellence, reputable research and innovation. It is one of the nine national research universities and among the top 10 comprehensive universities in Thailand by Quacquarelli Symonds’ ranking.

The university is proud of its distinguished record of achievements in teaching, research, development, and services to community. It has contributed significantly to the development of the country and consistently turned out well-qualified graduates of high-professional standing.
